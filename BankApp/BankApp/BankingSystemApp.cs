﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp
{
	internal class BankingSystemApp
	{
		private static Dictionary<string, double> accounts = new Dictionary<string, double>()
		{
			{ "123456", 1000.0 },
			{ "789101", 2000.0 }
		};

		public static double CheckBalance(string accountNumber)
		{
			if (accounts.TryGetValue(accountNumber, out double balance))
			{
				return balance;
			}
			else
			{
				Console.WriteLine("Account not found.");
				return 0;
			}
		}

		/// <summary>
		/// Patikrina ar tokios sąskaitos yra ir apie tai praneša jei tokios sąskaitos nėra
		/// patikrina ar pakanka pinigų sąskaitoje iš kurios pinigai pervedami
		/// nuima pinigus iš vienos sąskaitos ir prideda prie kitos ir išspausdina pranešimą apie pavykusią transakciją
		/// Praneša apie nepakankamas lėšas jei pinigų trūksta.
		/// </summary>
		/// <param name="fromAccountNumber"></param>
		/// <param name="toAccountNumber"></param>
		/// <param name="amount"></param>
		public static void TransferFunds(string fromAccountNumber, string toAccountNumber, double amount)
		{
			throw new NotImplementedException();
		}


		/// <summary>
		/// Naujas metodas išgryninti pinigus
		/// Patikrina ar tokia sąskaita yra
		/// Patikrina ar pakanka pinigų, jei ne išspausdina kad nepakanka
		/// Nuima pinigus ir išspausdina kiek pinų nuimta ir kiek liko
		/// </summary>
		/// <param name="accountNumber"></param>
		/// <param name="amount"></param>
		public static void WithdrawMoney(string accountNumber, double amount)
		{
			throw new NotImplementedException();
		}
	}
}
