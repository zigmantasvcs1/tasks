﻿namespace BankApp
{
	internal class Program
	{
		static void Main(string[] args)
		{
			bool continueBanking = true;

			while (continueBanking)
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1. Check balance");
				Console.WriteLine("2. Transfer funds");
				Console.WriteLine("3. Withdraw money");
				Console.WriteLine("4. Exit");

				var choice = Console.ReadLine();

				switch (choice)
				{
					case "1":
						Console.WriteLine("Enter your account number:");
						var accountNumber = Console.ReadLine();
						Console.WriteLine($"Your balance is: {BankingSystemApp.CheckBalance(accountNumber)}€");
						break;
					case "2":
						Console.WriteLine("Enter your account number:");
						var fromAccountNumber = Console.ReadLine();
						Console.WriteLine("Enter destination account number:");
						var toAccountNumber = Console.ReadLine();
						Console.WriteLine("Enter amount to transfer:");
						var amount = Convert.ToDouble(Console.ReadLine());
						BankingSystemApp.TransferFunds(fromAccountNumber, toAccountNumber, amount);
						break;
					case "3":
						Console.WriteLine("Enter your account number:");
						var accountNum = Console.ReadLine();
						Console.WriteLine("Enter amount to withdraw:");
						var withdrawAmount = Convert.ToDouble(Console.ReadLine());
						BankingSystemApp.WithdrawMoney(accountNum, withdrawAmount);
						break;
					case "4":
						continueBanking = false;
						break;
					default:
						Console.WriteLine("Invalid choice. Try again.");
						break;
				}
			}
		}
	}
}