﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalLibrary
{
	public class MedicalLibrarySystem
	{
		private static List<string> books = new List<string>()
		{
			"Human Anatomy",
			"Medical Microbiology",
			"Pharmacology Principles"
		};

		public static void ShowBooks()
		{
			Console.WriteLine("Available books:");
			foreach (var book in books)
			{
				Console.WriteLine(book);
			}
		}

		// TODO: Įgyvendinkite metodą, kuris prideda naują knygą į sąrašą.
		public static void AddBook(string book)
		{
			throw new NotImplementedException();
		}

		// TODO: Įgyvendinkite metodą, kuris ištrina knygą iš sąrašo pagal pavadinimą.
		public static void RemoveBook(string book)
		{
			throw new NotImplementedException();
		}

		// TODO: Įgyvendinkite metodą, kuris grąžina, ar knyga yra bibliotekos sąraše.
		public static bool BookExists(string book)
		{
			throw new NotImplementedException();
		}
	}
}
