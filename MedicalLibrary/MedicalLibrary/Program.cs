﻿namespace MedicalLibrary
{
	internal class Program
	{
		static void Main(string[] args)
		{
			bool continueLibrary = true;

			while (continueLibrary)
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1. Show available books");
				Console.WriteLine("2. Add a book");
				Console.WriteLine("3. Remove a book");
				Console.WriteLine("4. Check if a book exists");
				Console.WriteLine("5. Exit");

				var choice = Console.ReadLine();

				switch (choice)
				{
					case "1":
						MedicalLibrarySystem.ShowBooks();
						break;
					case "2":
						Console.WriteLine("Enter the book title:");
						var bookToAdd = Console.ReadLine();
						MedicalLibrarySystem.AddBook(bookToAdd);
						break;
					case "3":
						Console.WriteLine("Enter the book title:");
						var bookToRemove = Console.ReadLine();
						MedicalLibrarySystem.RemoveBook(bookToRemove);
						break;
					case "4":
						Console.WriteLine("Enter the book title:");
						var bookToCheck = Console.ReadLine();
						bool exists = MedicalLibrarySystem.BookExists(bookToCheck);
						Console.WriteLine(exists ? "Book exists." : "Book does not exist.");
						break;
					case "5":
						continueLibrary = false;
						break;
					default:
						Console.WriteLine("Invalid choice. Try again.");
						break;
				}
			}
		}
	}
}