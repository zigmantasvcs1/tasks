﻿namespace ShoppingCartApp
{
	internal class OnlineStore
	{
		private static Dictionary<string, double> products = new Dictionary<string, double>()
		{
			{ "Computer", 2000.0 },
			{ "Phone", 800.0 },
			{ "Headphones", 150.0 },
			{ "Mouse", 50.0 }
		};

		private static List<string> cart = new List<string>();

		public static void ShowProducts()
		{
			Console.WriteLine("Products:");
			foreach (var product in products)
			{
				Console.WriteLine($"{product.Key} - {product.Value}€");
			}
		}

		public static void AddToCart(string product)
		{
			if (products.ContainsKey(product))
			{
				cart.Add(product);
				Console.WriteLine($"Product {product} added to cart.");
			}
			else
			{
				Console.WriteLine("Such product doesn't exist.");
			}
		}

		public static void ShowCart()
		{
			Console.WriteLine("Your cart:");
			foreach (var product in cart)
			{
				Console.WriteLine(product);
			}
		}

		// TODO: Parašykite metodą kuris apskaičiuoja jūsų krepšelio bendrą pinigų sumą.
		public static double CalculateTotalAmount()
		{
			throw new NotImplementedException();
		}
	}
}
