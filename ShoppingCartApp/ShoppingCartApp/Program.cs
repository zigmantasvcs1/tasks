﻿namespace ShoppingCartApp
{
	internal class Program
	{
		static void Main(string[] args)
		{
			bool continueShopping = true;

			while (continueShopping)
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1. Show products");
				Console.WriteLine("2. Add product to cart");
				Console.WriteLine("3. Show cart");
				Console.WriteLine("4. Exit");

				var choice = Console.ReadLine();

				switch (choice)
				{
					case "1":
						OnlineStore.ShowProducts();
						break;
					case "2":
						Console.WriteLine("Enter the product name:");
						var product = Console.ReadLine();
						OnlineStore.AddToCart(product);
						break;
					case "3":
						OnlineStore.ShowCart();
						break;
					case "4":
						continueShopping = false;
						break;
					default:
						Console.WriteLine("Invalid choice. Try again.");
						break;
				}

				if (choice == "2" || choice == "3")
				{
					Console.WriteLine("Continue shopping? (yes/no)");
					var response = Console.ReadLine()?.ToLower();

					if (response == "no")
					{
						continueShopping = false;
					}
				}
			}
		}
	}
}